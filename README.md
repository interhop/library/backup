Backup with Duplicity
==

# Overview
- Fonction / Categorie : backup
- site officiel :  [https://duplicity.gitlab.io/duplicity-web/](https://duplicity.gitlab.io/duplicity-web/)
- Source code: 
  - [https://gitlab.com/duplicity/duplicity](https://gitlab.com/duplicity/duplicity)
- documentation
  - [https://duplicity.gitlab.io/duplicity-web/docs.html](https://duplicity.gitlab.io/duplicity-web/docs.html)
  - [https://linux.die.net/man/1/duplicity](https://linux.die.net/man/1/duplicity)
- Licence : GNU GPLv2
- Version testée : 0.8.20 
- Socle interministeriel : 0
- Catalogue GouvTech : 0
- Comptoir du libre : 0
- Framalibre : 0

# Synopsis

We will learn how to backup our data. 

We will use Object Storage service (like Scaleway) and Duplicity. 

We use Scaleway because [their discovery offer](https://www.scaleway.com/en/object-storage/) allows 75 GB of free storage and transfer each month. Then the rate applied beyond these 75 GB is 0.01 € / GB per month per volume stored, and 0.01 € / GB per volume transferred.

Our objectives are to:
- Make production-ready automatic and incremental backups of our data
- Restore backups
- Preserve a robust layer of security and privacy with an asymmetrical encryption

# Duplicity

Duplicity is a free and open-source tool that backs up folders to a remote server.

Duplicity backs directories by producing encrypted tar-format volumes and uploading them to a remote or local file server. 

Because duplicity uses librsync, the incremental archives are space efficient and only record the parts of files that have changed since the last backup. 

Because duplicity uses GnuPG to encrypt and/or sign these archives, they will be safe from spying and/or modification by the server.

# Quickstart
- copy and edit ```.configrc_tempate```, ```backup_template.sh```, ```restore_template.sh```
- generate gpg key : ```gpg2 --full-generate-key``` 
- create s3 bucket
- edit crontab

# Installation

## Requirements

These software should be installed :
- gpg2 (or gpg for MacOs, Because Homebrew  link gpg2 binary as gpg.)
- python3
- have sudo rights

## Scaleway

### Create you API key
- Log in to the Scaleway Console : [https://console.scaleway.com/](https://console.scaleway.com/)

- Then  generated your API key : "Project Dashboard" > "Credentials" > "Generate new API key".
There is two key : ```Access Key``` and ```Secret Key```

### Create a bucket in Scaleway’s Object Storage
- Log in to the Scaleway Console : https://console.scaleway.com/
- Click Object Storage from the left side menu. The Storage page lists all your buckets. At first, your list is empty as you have not created any bucket yet.
- Click Create a Bucket to create a bucket that will store your objects.
- Name your bucket and validate your bucket creation. A bucket name must be unique and contain only alphanumeric and lowercase characters.

## Generate a GPG Key with a passphrase

To generate the GPG key, launch this command.

```bash
sudo -i
gpg2 --full-generate-key # validate default values
```
Enter and remember a passphrase. You will be asked to define the characteristics of your keys. We will go with default settings:
- What kind of key you want: (1) RSA and RSA (default)
- What keysize do you want: (3072)
- How long the key should be valid: 0 = key does not expire
- GPG will then ask how to call your key, an address and a description.

You need to use the GPG Key fingerprint, it could be an 8, 16 or 40 char long hash. 

You can also find the fingerprint of your key with the command :

```bash
sudo gpg2 --list-keys
```

You have to export you private key with the command : 

```bash
sudo gpg2 --output private.pgp --armor --export-secret-key <the-gpg-key-id>
```

Store the bellow informations in a secure place. 
- the gpg private key
- the gpg passphrase
- the cloud provider secret keys : ```Access Key``` and ```Secret Key```

To store these informations, Interhop use Vaultwarden which is a password manager.

## Install the free and open-source backup solution : Duplicity

### Clone our library
```bash
sudo -i
cd /opt
git clone https://framagit.org/interhop/library/backup
cd backup
```

### Install duplicity

- MacOs : ```brew install duplicity```
- Ubuntu : ```apt-get install duplicity```
- pip : ```pip install -r requirements.py```

# Backups

## Configure ```.configrc``` and ```backup.sh``` files

### ```.configrc```

- Copy ```.configrc_template``` to ```.configrc```
```cp .configrc_template .configrc```
- Change the variable : ```AWS_ACCESS_KEY_ID```, ```AWS_SECRET_ACCESS_KEY```, ```SOURCE_1``` (```SOURCE_2``` etc), ```BUCKET``` in ```.configrc``` file
- Change rights
```bash
chmod 666 .configrc
```

### ```backup.sh```

- Copy ```backup_template.sh``` to create ```backup.sh```
```cp backup_template.sh backup.sh```
- Change the variable : ```<full-path-to>``` in ```backup.sh``` file
- Add the source folders you want. By default there is only ```SOURCE_1```.
- Change rights
```bash
chmod 777 backup.sh
```

## Test the backup script

- Run the script ```./backup.sh``` to make sure the configuration is correctly set. 

Check Scaleway’s bucket on the web interface and the logs with the following command: ```cat /var/log/duplicity/logfile-recent.log```

## Automatize backups with crontab

```bash
sudo -i
crontab -e
```
And add : 
```
PATH=/usr/local/bin:/usr/bin:/bin:/other/path/to/bin
*/15 * * * * </path/to>/backup.sh```
```

The script ``backup.sh`` is launched every 15 minutes (to check the access to an internet connection). However, the data replication (duplicity) is actually performed only once a day.

You can list your tasks :

```bash
crontab -l
```

# Restore

## Configure```restore.sh```

- Copy ```restore_template.sh``` to create ```restore.sh```
```cp restore_template.sh restore.sh```
- Change the variable : ```<full-path-to>``` in ```restore.sh``` file
- Change rights
```bash
chmod 700 restore.sh
```

## Recover data

- Recover the data you uploaded in the previous section with the following command :

```bash
./restore.sh 0D /tmp/backup-recovery-test/
```

You can also recover one specific file with the following format from a backup 5 days ago with:

```bash
./restore.sh 5D <file_name> /tmp/backup-recovery-test/
```
