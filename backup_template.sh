#!/bin/bash

    ulimit -n 1024

    # Variables
    CURRENTLY_BACKUP=$(ps -ef | grep duplicity  | grep python | wc -l)
    CONNECTION_TEST=$(ping -c 1 -q interhop.org >&/dev/null; echo $?) #https://www.golinuxcloud.com/commands-check-if-connected-to-internet-shell/
    DATE=`date +%Y-%m-%d`
    FILE=/tmp/backup/$DATE
    FORCE_MODE="$1"

    # Functions : 
    ## Duplicity function : duplicate data and erase to old one
    ## you may want to add other SOURCE folder like this
    ##   --include ${SOURCE_1} --include ${SOURCE_2} \
    duplicity_function () {
      log ">>> creating and uploading backup to c14 cold storage"
      duplicity \
          incr --full-if-older-than ${FULL_BACKUP_TIME} \
          --asynchronous-upload \
          --s3-use-glacier \
          --encrypt-key=${GPG_KEY} \
          --sign-key=${GPG_KEY} \
          --include ${SOURCE_1} \
          --exclude '**' /  ${BUCKET} >> ${LOGFILE_RECENT} 2>&1

      # Keep only three full - purge older full
      log ">>> removing old backups"
      duplicity remove-all-but-n-full 3 \
         --force  \
         ${BUCKET} >> ${LOGFILE_RECENT} 2>&1
    }

    # Create directory if not exists
    mkdir -p /var/log/duplicity
    mkdir -p /tmp/backup

    # Source conf
    source <full-path-to>/.configrc

    if [ -n "$FORCE_MODE" ] && [ "$FORCE_MODE" == "--force" ] ; then
      # Clear the recent log file
      cat /dev/null > ${LOGFILE_RECENT}
      duplicity_function
      cat ${LOGFILE_RECENT}

    elif [ "$CURRENTLY_BACKUP" -eq 0 ] && ! [ -f "$FILE" ] && ! [ "$CONNECTION_TEST" -eq 0 ] ; then
      # Clear the recent log file
      cat /dev/null > ${LOGFILE_RECENT}
      duplicity_function
      cat ${LOGFILE_RECENT} >> ${LOGFILE}

      # Create file to avoid new daily backup
      touch "$FILE"

    elif [ "$CONNECTION_TEST" -eq 0 ]  ; then
      log ">>> no internet connexion"

    elif [ -f "$FILE" ] ; then
      log ">>> replication has already taken place today"

    fi
